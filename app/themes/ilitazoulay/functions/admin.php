<?php

 /**
 * This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
 */
 add_theme_support( 'post-thumbnails' );

/**
 * Ajout custom css
 */

add_action( 'admin_enqueue_scripts', 'load_admin_style' );
function load_admin_style() {
  wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/admin-style.css', false, '1.0.0' );
}

/**
 * Remove admin menus
 */
function remove_menus(){
  remove_menu_page( 'edit-comments.php' );          //Comments
}
add_action( 'admin_menu', 'remove_menus' );

/**
 * Enlève howdy
 */
add_filter( 'gettext', 'change_howdy_text', 10, 2 );
function change_howdy_text( $translation, $original ) {
  if( 'Howdy, %1$s' == $original )
     $translation = '%1$s';
  return $translation;
}

 /**
 * Ajoute éditeur de menu
 */
add_theme_support( 'menus' );



/**
 * Rename posts
 */

function uo_change_post_label() {
  global $menu;
  global $submenu;
  $menu[5][0] = 'Projects';
  $submenu['edit.php'][5][0] = 'Projects';
  $submenu['edit.php'][10][0] = 'Add a project';
  echo '';
}
function uo_change_post_object() {
  global $wp_post_types;
  $labels = &$wp_post_types['post']->labels;
  $labels->name = 'Projects';
  $labels->singular_name = 'Project';
  $labels->add_new = 'Add a project';
  $labels->add_new_item = 'Add a project';
  $labels->edit_item = 'Edit a project';
  $labels->new_item = 'project';
  $labels->view_item = 'View project';
  $labels->search_items = 'Find';
  $labels->not_found = 'No project found';
  $labels->not_found_in_trash = 'No project found';
  $labels->all_items = 'All the projects';
  $labels->menu_name = 'Projects';
  $labels->name_admin_bar = 'Projects';
}
 
add_action( 'admin_menu', 'uo_change_post_label' );
add_action( 'init', 'uo_change_post_object' );


/**
 * Ajout custom post 'Texts'
 */
add_action('init', 'create_custom_post_texts');

function create_custom_post_texts () {
  register_post_type(
    'texts',
    array(
      'labels' => array(
        'name' => 'Texts',
        'singular_label' => 'Text',
        'add_new_item' => 'Add New text',
        'edit_item' => 'Edit text',
        'new_item' => 'New text',
        'view_item' => 'View text',
        'search_items' => 'Search Text',
        'not_found' => 'No texts found',
        'not_found_in_trash' => 'Now texts found in trash'
      ),
      'public' => true,
      'exclude_from_search' => true,
      'show_ui' => true,
      'hierarchical' => false,
      'supports' => array('title', 'editor', 'excerpt'),
      'menu_position' => 5,
      'has_archive' => 'text',
      'rewrite' => array(
        'slug' => 'texts',
        'with_front' => false
      ),
    )
  );
}

/**
 * Ajout custom post 'Publications'
 */
add_action('init', 'create_custom_post_publication');

function create_custom_post_publication () {
  register_post_type(
    'publications',
    array(
      'labels' => array(
        'name' => 'Publications',
        'singular_label' => 'Publication',
        'add_new_item' => 'Add New publication',
        'edit_item' => 'Edit publication',
        'new_item' => 'New publication',
        'view_item' => 'View publication',
        'search_items' => 'Search Publication',
        'not_found' => 'No publications found',
        'not_found_in_trash' => 'Now publications found in trash'
      ),
      'public' => true,
      'exclude_from_search' => true,
      'show_ui' => true,
      'hierarchical' => false,
      'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
      'menu_position' => 5,
      'has_archive' => 'publication',
      'rewrite' => array(
        'slug' => 'publications',
        'with_front' => false
      ),
    )
  );
}

/**
 * Ajout custom post 'Press'
 */
add_action('init', 'create_custom_post_press');

function create_custom_post_press () {
  register_post_type(
    'press',
    array(
      'labels' => array(
        'name' => 'Press',
        'singular_label' => 'Press',
        'add_new_item' => 'Add New article',
        'edit_item' => 'Edit article',
        'new_item' => 'New article',
        'view_item' => 'View article',
        'search_items' => 'Search article',
        'not_found' => 'No article found',
        'not_found_in_trash' => 'Now article found in trash'
      ),
      'public' => true,
      'exclude_from_search' => true,
      'show_ui' => true,
      'hierarchical' => false,
      'supports' => array('title', 'editor', 'excerpt'),
      'menu_position' => 6,
      'has_archive' => 'article',
      'rewrite' => array(
        'slug' => 'article',
        'with_front' => false
      ),
    )
  );
}

/**
 * Ajout custom post 'News'
 */
add_action('init', 'create_custom_post_exhibition');

function create_custom_post_exhibition() {
  register_post_type(
    'exhibitions',
    array(
      'labels' => array(
        'name' => 'Exhibitions',
        'singular_label' => 'Exhibition',
        'add_new_item' => 'Add exhibition',
        'edit_item' => 'Edit exhibition',
        'new_item' => 'New exhibition',
        'view_item' => 'View exhibition',
        'search_items' => 'Search exhibition',
        'not_found' => 'No exhibition found',
        'not_found_in_trash' => 'Now exhibition found in trash'
      ),
      'public' => true,
      'exclude_from_search' => true,
      'show_ui' => true,
      'hierarchical' => false,
      'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
      'menu_position' => 7,
      'has_archive' => 'exhibition',
      'rewrite' => array(
        'slug' => 'exhibition',
        'with_front' => false
      ),
    )
  );
}

/**
 * Réécriture de l'image dans le textarea
 */

function insert_image($html, $id, $caption, $title, $align, $url) {
  if ($caption):
    $cap = '<figcaption>'.esc_attr($caption).'</figcaption>';
  else:
    $cap = '';
  endif;
  $html5 = '<figure><img src='.esc_attr($url).' alt="$title">'.$cap.'</figure>';
  return $html5;
}
add_filter( 'image_send_to_editor', 'insert_image', 10, 9 );




/**
 * Tinymce
 */

// Add Formats Dropdown Menu To MCE
if ( ! function_exists( 'wpex_style_select' ) ) {
  function wpex_style_select( $buttons ) {
    array_push( $buttons, 'styleselect' );
    return $buttons;
  }
}
add_filter( 'mce_buttons', 'wpex_style_select' );

// Add new styles to the TinyMCE "formats" menu dropdown
if ( ! function_exists( 'wpex_styles_dropdown' ) ) {
  function wpex_styles_dropdown( $settings ) {

    // Create array of new styles
    $new_styles = array(
      array(
        'title' => __( 'Custom Styles', 'wpex' ),
        'items' => array(           
          array(
            'title'   => __('Intro','wpex'),
            'selector'  => 'p',
            'classes' => 'intro'
          ),
        ),
      ),
    );

    // Merge old & new styles
    $settings['style_formats_merge'] = true;

    // Add new styles
    $settings['style_formats'] = json_encode( $new_styles );

    // Return New Settings
    return $settings;

  }
}
add_filter( 'tiny_mce_before_init', 'wpex_styles_dropdown' );


/**
 * Add stylesheet to Tinymce 
 */

if ( ! function_exists('tdav_css') ) {
  function tdav_css($wp) {
    $wp .= ',' . get_bloginfo('stylesheet_url');
  return $wp;
  }
}
add_filter( 'mce_css', 'tdav_css' );


// Add custom format to TinyMCE editor

function tw_tiny_mce_before_init($init) {
  $init['block_formats'] = 'Heading level 2=h2;Heading level 3=h3;Heading level 4=h4;Body Text=p;Address=address;Quote=blockquote';
  
  return $init;
}
add_filter('tiny_mce_before_init', 'tw_tiny_mce_before_init' );

