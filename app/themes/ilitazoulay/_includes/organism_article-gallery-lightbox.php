<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 js-packery gallery gallery-lightbox <?php echo $classes; ?>"
      itemscope itemtype="http://schema.org/ImageGallery" 
      data-packery-options='{ "itemSelector": ".item", "isOriginTop": <?php echo $origin_top; ?> }'>
    <?php
       // Images
       if(get_field('gallery') ):
         $j = 1;
         $total_items          = count(get_field('gallery'));
         $random_item          = rand(1, $total_items-1);
         $is_home              = is_front_page();
         $project_size_max     =  1200; // Taille max d'un projet en cm
         $project_size_medium  =  150; // Taille max d'un projet en cm (= pas de taille)
         $project_size_min     =  100; // Taille minimum d'un projet en cm

         while ( has_sub_field('gallery') ) :    
           $legend      = get_sub_field('legend');
           $width       = get_sub_field('width');
           $height      = get_sub_field('height');
           $image       = get_sub_field('picture');
           if(!empty($width) && $width > 0 && !empty($height) && $height > 0):
             
             if($width > $height):
                // Horizontal
                $size_coef  = $width / $project_size_max;
                if($size_coef < 0.15): $size_coef = 0.15; endif;
             else:
                // Vert.
                $size_coef   =  $height / $project_size_max;
                if($size_coef < 0.30): $size_coef = 0.30; endif;
             endif;

             
           else:
             $size_coef   = $project_size_medium / $project_size_max;
           endif;
           $image_title   = $image['title'];
           $image_alt     = $image['alt'];
           $full_width    = $image['width'];
           $full_height   = $image['height'];     
           $size          = "large"; 
           $thumb         = $image['sizes'][ "medium" ]; // thumbnail, medium, large, full or custom size
           $thumb_width   = round($image['sizes'][ "medium-width" ] * $size_coef);
           $thumb_height  = round($image['sizes'][ "medium-height" ] * $size_coef);      
           //print_r($image);
           $image          = $image['url'];
           $sounds         = get_sub_field('sounds');
           $sounds_output  = "";
           if(!empty($sounds)):
             while ( has_sub_field('sounds') ) :
               $title      = get_sub_field('titre');
               $mp3        = get_sub_field('mp3'); 
               $mp3_url    = $mp3['url']; 
               $sounds_output .=  '<div class="sound"><a href="'.$mp3_url.'" title="'.$title.'" class="sm2_button">'.$title.'</a><span>'.$title.'</span></div>';           
             endwhile;
           endif;
    ?>
       <figure class="item" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

          <a 
          href="<?php echo $image; ?>" 
          itemprop="contentUrl" 
          data-size="<?php echo $full_width; ?>x<?php echo $full_height; ?>" 
          data-index="<?php echo $j; ?>" 
          data-med="<?php echo $thumb; ?>" 
          data-med-size="data-size="<?php echo $thumb_width; ?>x<?php echo $thumb_height; ?>"" 
          >
            <span><?php echo $j; ?></span>
             <img 
              data-original="<?php echo $thumb; ?>" 
              alt="<?php echo $title; ?> — <?php echo $j ;?>" 
              class="lazy" 
              width="<?php echo $thumb_width; ?>" 
              height="<?php echo $thumb_height; ?>"
              style="width: <?php echo $thumb_width; ?>px; height: <?php echo $thumb_height; ?>px;" 
              itemprop="thumbnail"/>
          </a>
          <figcaption>
            <?php echo $legend; ?><br />
            <?php echo $sounds_output; ?>       
          </figcaption>
       </figure>       
    <?php $j++; endwhile; ?> 
    <?php endif; ?> 
    </div>
  </div>
</div>