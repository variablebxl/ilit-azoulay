<div class="gallery gallery-slide gallery-slide-<?php echo $gallery_slide_num; ?> dragscroll">
  <div class="arrows">
    <a href="#" class="arrow arrow-left">Left</a>
    <a href="#" class="arrow arrow-right">Right</a>
  </div>
<?php
   // Images
   if(get_field('gallery') ):
     $j = 1;
     $total_items       = count(get_field('gallery'));
     $random_item       = rand(1, $total_items-1);
     $is_home           = is_front_page();
     $project_size_max    =  1200; // Taille max d'un projet en cm
     $project_size_medium  =  200; // Taille max d'un projet en cm
     $project_size_av      =  100; // Taille moyenne d'un projet en cm

     while ( has_sub_field('gallery') ) :    
       $legend      = get_sub_field('legend');
       $width       = get_sub_field('width');
       $height      = get_sub_field('height');
       $image       = get_sub_field('picture');
       if(!empty($width) && $width > 0):
         $size_coef   = $width / $project_size_max;
       else:
         $size_coef   = $project_size_medium / $project_size_max;
       endif;
       $image_title   = $image['title'];
       $image_alt     = $image['alt'];
       $full_width    = $image['width'];
       $full_height   = $image['height'];     
       $size          = "large";       
       //print_r($image);
       $image         = $image['url'];
       $direction     = ($full_width < $full_height)? "vertical" : "";
?>
   <figure class="item <?php echo $direction; ?>">
      <div>
        <span><?php echo $j; ?></span>
        <img data-original="<?php echo $image; ?>" alt="<?php echo $title; ?> — <?php echo $j ;?>" class="lazy-h-<?php echo $gallery_slide_num; ?>" width="<?php echo $full_width; ?>" height="<?php echo $full_height; ?>" style="width: <?php echo $full_width; ?>px; height: <?php echo $full_height; ?>px;" />
        <figcaption><?php echo $legend; ?></figcaption>
      </div>
   
   </figure>      
<?php $j++; endwhile; ?> 
<?php endif; ?> 
</div>
<?php
$gallery_slide_num++;
?>