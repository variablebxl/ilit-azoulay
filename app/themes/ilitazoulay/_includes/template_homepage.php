  <?php
    $i = 0;
    $args_homepage = array(
      'numberposts' => -1    
    );
    $the_query = new WP_Query($args_homepage);
  ?>

<div class="panel-full" id="panel-full"></div>
<main>
  <?php
    if($the_query->have_posts()):
    while ( $the_query->have_posts() ) : $the_query->the_post();
      $title           = get_the_title();
      $text            = get_the_content();
      $year            = get_field("year");
      $credits         = get_field("credits");
      $gallery_type    = get_field("gallery_type"); // Lightbox — Horizontal slide — Grow
      $class_alignment = get_field('gallery_alignment'); // Top  — Bottom
      
      switch ($class_alignment):
        case  "Top" : 
          $origin_top = "true";
          break;
        default       :  // Bottom
          $origin_top = "false"; 
          break;
      endswitch;

      // Classes      
      $class_margins   = get_field('margins'); // No — Small — Medium — Large — Extra-large
      switch ($class_margins):
      case  "No" : 
        $classes = "margin-none ";
        break;
      case  "Small" : 
        $classes = "margin-sm ";
        break;
      case  "Large" : 
        $classes = "margin-lg ";
        break; 
        case  "Extra-large" : 
        $classes = "margin-xl ";
        break;              
      default       :  // Medium
        $classes = "margin-md "; 
        break;
      endswitch;

      $class_alignment_horizontal   = get_field('gallery_alignment_horizontal'); // Left - Center
      if(!empty($class_alignment_horizontal)):
        switch ($class_alignment_horizontal): 
          case  "Left" : 
          $classes .= "gallery-align-left ";
          break;              
        default       :  // Center
          $classes .= "gallery-align-center "; 
          break;
        endswitch;
      endif;

      $class_alignment_grow   = get_field('gallery_alignment_grow'); // Top - Middle - Bottom
      if(!empty($class_alignment_grow)):
        switch ($class_alignment_grow): 
          case  "Middle" : 
          $classes .= "item-align-middle ";
          break;  
          case  "Bottom" : 
          $classes .= "item-align-bottom ";
          break;                        
        default       :  // Top
          $classes .= "item-align-top "; 
          break;
        endswitch;
      endif;
  ?> 
   <article <?php if($gallery_type == 'Horizontal slide'){echo 'class="slide-container"';}; ?>>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-4 col-md-offset-1">
              <div class="block block-headline-byline">
                <hr>
                <h2 class="b-title"><?php echo $title; ?></h2>
                <time datetime="2015"><?php echo $year; ?></time>   
                <?php 
                  if(!empty($text)): 
                    echo '<p><a href="#" class="button more">&#9618;&#9618;</a></p>';
                  endif; 
                ?>                
              </div>
            </div>
            <?php
              if(!empty($text)):
                require('molecule_info-panel.php'); 
              endif;
            ?>
         </div>
      </div>
      <?php 
        if($gallery_type == "Lightbox"):
          require('organism_article-gallery-lightbox.php');
        elseif($gallery_type == "Horizontal slide"):
          require('organism_article-gallery-slide.php');
        else:
          require('organism_article-gallery-grow.php');
        endif;
      ?>
  </article>
  <?php endwhile; ?>
  <?php endif; ?>
  <?php // Structure of PhotoSwipe ?>
   <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
       <div class="pswp__bg"></div>
       <div class="pswp__scroll-wrap">
           <div class="pswp__container">
               <div class="pswp__item"></div>
               <div class="pswp__item"></div>
               <div class="pswp__item"></div>
           </div>
           <div class="pswp__ui pswp__ui--hidden">
               <div class="pswp__top-bar">
                   <div class="pswp__counter"></div>
                   <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                   <!-- <button class="pswp__button pswp__button--share" title="Share"></button> }}-->
                   <!-- <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> -->
                   <!-- <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button> -->
                   <div class="pswp__preloader">
                       <div class="pswp__preloader__icn">
                         <div class="pswp__preloader__cut">
                           <div class="pswp__preloader__donut"></div>
                         </div>
                       </div>
                   </div>
               </div>
               <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                   <div class="pswp__share-tooltip"></div> 
               </div>
               <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
               </button>
               <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
               </button>
               <div class="pswp__caption">
                  <div class="pswp__caption__center"></div>
               </div>
           </div>
       </div>
   </div>
</main>