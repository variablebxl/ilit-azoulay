<div class="container-fluid">
  <div class="row">
    <div class="gallery gallery-grow col-md-12 <?php echo $classes; ?>">
    <?php
    // Images
    if(get_field('gallery') ):
    $j = 1;
    $total_items          = count(get_field('gallery'));
    $random_item          = rand(1, $total_items-1);
    $is_home              = is_front_page();
    $project_size_max     =  1200; // Taille max d'un projet en cm
    $project_size_medium  =  300; // Taille max d'un projet en cm
    $project_size_min     =  100; // Taille minimum d'un projet en cm

    while ( has_sub_field('gallery') ) :    
      $legend      = get_sub_field('legend');
      $width       = get_sub_field('width');
      $height      = get_sub_field('height');
      $image       = get_sub_field('picture');
      if(!empty($width) && $width > 0 && !empty($height) && $height > 0):
        
        if($width > $height):
           // Horizontal
           $size_coef  = $width / $project_size_max;
           if($size_coef < 0.15): $size_coef = 0.15; endif;
        else:
           // Vert.
          $size_coef   =  $height / $project_size_max;
           if($size_coef < 0.30): $size_coef = 0.30; endif;
        endif;
        
      else:
        $size_coef   = $project_size_medium / $project_size_max;
      endif;
      $image_title   = $image['title'];
      $image_alt     = $image['alt'];
      $full_width    = $image['width'];
      $full_height   = $image['height'];     
      $thumb         = $image['sizes'][ "medium" ]; // thumbnail, medium, large, full or custom size
      $image_full    = $image['sizes'][ "large" ]; // thumbnail, medium, large, full or custom size  
      $thumb_width   = round($image['sizes'][ "medium-width" ] * $size_coef);
      $thumb_height  = round($image['sizes'][ "medium-height" ] * $size_coef);      
      //print_r($image);
      $image        = $image['url'];
    ?>
       <figure class="item small-to-big small">
          <div>
             <a href="<?php echo $image_full; ?>" class="small">
                <span><?php echo $j; ?></span>
                <img data-original="<?php echo $thumb; ?>" alt="<?php echo $title; ?> — <?php echo $j ;?>" class="lazy" width="<?php echo $thumb_width; ?>" height="<?php echo $thumb_height; ?>" style="width: <?php echo $thumb_width; ?>px; height: <?php echo $thumb_height; ?>px;"/>
                <figcaption><?php echo $legend; ?></figcaption>
             </a>
          </div>
         
       </figure>    
    <?php $j++; endwhile; ?> 
    <?php endif; ?> 
    </div>
  </div>
</div>


