<?php 
  while (have_posts()) : the_post(); 
   $infos = get_field('infos');
?>
<main class="infos">
  <div class="container-fluid">
    <div class="row">
    <div class="col-md-4 col-md-offset-1">
      <div class="block block-headline-byline">
        <hr>
        <h2 class="b-title">Contact</h2>
      </div>               
      <article class="contact">
        <?php the_content(); ?>
      </article>
    </div>
    <div class="col-md-4 col-md-offset-2">
      <div class="block block-headline-byline">
        <hr>
        <h2 class="b-title">Infos</h2>
      </div>               
      <article>
        <?php echo $infos; ?>
      </article>
    </div>
    </div>
   </div>
</main>
<?php 
  endwhile;