<?php
  $i = 0;
  $args_texts = array(
    'numberposts' => -1,   
    'post_type'   => 'texts',   
  );
  $the_query = new WP_Query($args_texts);
?>

<main>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4 col-md-offset-1">
        <div class="block block-headline-byline">
          <hr>
          <h2 class="b-title">Texts</h2>
        </div>            
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">  
      <?php
        if($the_query->have_posts()):
        while ( $the_query->have_posts() ) : $the_query->the_post();
          $title           = get_the_title();
          $link            = get_permalink();
          $text            = get_the_content();
          $author          = get_field("author");
          $translation     = get_field("translation");
          $published        = get_field("published");
      ?>
        <article class="block-text col-md-10 col-md-offset-1">
          <div class="block block-headline-text">
          <a href="<?php echo $link; ?>">
            <h2 class="headline"><?php echo $title; ?></h2>
          </a>
          <ul>
            <?php if(!empty($author)): ?>
            <li>by <?php echo $author; ?></li>
            <?php endif; ?>
            <?php if(!empty($published)): ?>
            <li>Published in:&nbsp;<?php echo $published; ?></li>
            <?php endif; ?>
            <?php if(!empty($translation)): ?>
            <li>Read the <a href="<?php echo $translation; ?>" target="_blank">Hebrew Translation</a></li>
            <?php endif; ?>
          </ul>
          </div>            
        </article>
      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</main>