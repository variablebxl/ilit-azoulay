<?php 
  while (have_posts()) : the_post(); 
  $post_id          = $post->ID;
  $title           = get_the_title();
  $link            = get_permalink();
  $text            = get_the_content();
  $image_id        = get_post_thumbnail_id();
  $image_array     = wp_get_attachment_image_src($image_id , "medium");
  $image_url       = $image_array[0];
  $image_width     = $image_array[1]; 
  $image_height    = $image_array[2];
  $introduction    = get_field('introduction');
  $infos           = get_field('infos');
  $where           = get_field('where');
?>
  <main class="publications">
    <div class="container-fluid">
       <div class="row">
          <div class="col-md-4 col-md-offset-1">
             <div class="block block-headline-byline">
               <hr>
                <h2 class="b-title">Publications</h2>
             </div>
          </div>
       </div>
       <div class="row">
          <div class="col-md-4 col-md-offset-1">
            <?php if(!empty($image_url)): ?>
            <img src="<?php echo $image_url; ?>" alt="<?php echo $title; ?>">
            <?php endif; ?>
          </div>
          <div class="col-md-6">
             <h2><?php echo $title; ?></h2>
             <p><?php echo $introduction; ?></p>
          </div>
       </div>
    </div>

    <div class="slide-container">
    <?php require('organism_article-gallery-slide-publications.php'); ?>
    </div>
    <div class="container-fluid">
       <div class="row">
          <div class="col-md-6 col-md-offset-1">
            <?php echo $text; ?>
          </div>
          <div class="col-md-3 col-md-offset-1">
             <div class="infos-link">
                <h3>Where to get it</h3>
                <?php echo $where; ?>
             </div>
             <div class="infos-publications">
              <?php echo $infos; ?>
             </div>
          </div>
       </div>
    </div>
  </main>
<?php endwhile; ?>




