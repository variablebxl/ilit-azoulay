<div class="gallery gallery-slide gallery-slide-<?php echo $gallery_slide_num; ?> dragscroll">
  <div class="arrows">
    <a href="#" class="arrow arrow-left">Left</a>
    <a href="#" class="arrow arrow-right">Right</a>
  </div>
<?php
   // Images
   if(get_field('pictures') ):
     $j = 1;
     $total_items       = count(get_field('pictures'));
     $random_item       = rand(1, $total_items-1);
     $is_home           = is_front_page();
     $project_size_max    =  1200; // Taille max d'un projet en cm
     $project_size_medium  =  200; // Taille max d'un projet en cm
     $project_size_av      =  100; // Taille moyenne d'un projet en cm

     while ( has_sub_field('pictures') ) :    
       $image       = get_sub_field('image_file');
       $image_title   = $image['title'];
       $image_alt     = $image['alt'];
       $full_width    = $image['width'];
       $full_height   = $image['height'];     
       $size          = "large";       
       //print_r($image);
       $image        = $image['url'];
?>
   <figure class="item">
      <div>
        <img src="<?php echo $image; ?>" alt="<?php echo $title; ?> — <?php echo $j ;?>" class="" width="<?php echo $full_width; ?>" height="<?php echo $full_height; ?>" style="width: <?php echo $full_width; ?>px; height: <?php echo $full_height; ?>px;" />
      </div>
   </figure>      
<?php $j++; endwhile; ?> 
<?php endif; ?> 
</div>