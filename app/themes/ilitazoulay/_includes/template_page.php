<?php 
  while (have_posts()) : the_post(); 
?>
<div class="category-title">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-9 col-md-offset-3">
        <h3 class="section-title"><?php the_title(); ?></h3>
      </div>
    </div>
  </div>
</div>
<main class="container-fluid">
  <div class="row">
     <div class="col-md-2">
      <aside class="aside-page aside">
        <ul class="nav-secondary">
          <!-- <li><a href="<?php echo $siteurl; ?>/distribution" class="<?php if(is_page('distribution')) echo 'current-menu-item'; ?>">Distribution</a></li> -->
          <li><a href="<?php echo $siteurl; ?>/about" class="<?php if(is_page('about')) echo 'current-menu-item'; ?>">About us</a></li>
          <li><a href="<?php echo $siteurl; ?>/contact" class="<?php if(is_page('contact')) echo 'current-menu-item'; ?>">Contact us</a></li>
        </ul>
      </aside>
    </div>
    <div class="col-md-9 col-md-offset-1">
      <article>
        <?php the_content(); ?>
      </article>
    </div>
  </div>
</main>
<?php 
  endwhile;