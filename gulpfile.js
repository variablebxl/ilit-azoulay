var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    beautify = require('gulp-beautify'),
    cssmin = require('gulp-cssmin'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename');
    autoprefixer = require('gulp-autoprefixer'),
    shell = require('gulp-shell');

gulp.task('js', function() {
  gulp.src(['assets/js/*.js'])
  // Concatène tous les fichiers js en 1
  .pipe(concat('scripts.js'))
  // Indente 
  .pipe(beautify({indentSize: 2}))
  // Sauve le fichier dans Dist 
  .pipe(gulp.dest("assets/dist"))
  // Renomme le fichier avec .min
  .pipe(rename({suffix: '.min'}))
  // Compresse le fichier 
  .pipe(uglify())
  // Sauve le fichier compressé dans Dist 
  .pipe(gulp.dest('assets/dist/'))
});

gulp.task("sass", function(){ 
  gulp.src('assets/css/*.scss')
  .pipe(sourcemaps.init())
  // Compile sass avec les commentaires dans la source 
  .pipe(sass({ 
    style: 'expanded',
    sourceComments: 'normal'
  }))
  // Ajoute des préfixes automatiquement    
  .pipe(autoprefixer())
  // Commente le code pour debug avec firebug
  .pipe(sourcemaps.write())
  // Sauve le fichier dans Dist 
  .pipe(gulp.dest("assets/dist"))
  // Renomme le fichier avec .min
  .pipe(rename({suffix: '.min'}))
  // Compresse le fichier 
  .pipe(cssmin())
  // Sauve le fichier dans Dist 
  .pipe(gulp.dest('assets/dist'))
});

gulp.task('patternlab', shell.task([
  // Lance Patternalb Watch 
  'patternlab/core/scripts/startWatcher.command'
]));

gulp.task('watch', function() {
  gulp.watch('assets/js/**/*.js',['js']);
  gulp.watch('assets/css/**/*.scss',['sass']);
});

gulp.task('default', ['watch', 'patternlab']);