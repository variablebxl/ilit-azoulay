<?php


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


if(strstr($_SERVER["HTTP_HOST"], "ilitazoulay.dev") != false){ // Local dev server
  // URLS
  define ('SITEURL', 'http://' . $_SERVER['HTTP_HOST']);
  define ('WP_CONTENT_FOLDERNAME', 'app');
  define ('WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME);
  define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/app' );

  // Enable WP_DEBUG mode
  define('WP_DEBUG', true);
  // Enable Debug logging to the /wp-content/debug.log file
  define('WP_DEBUG_LOG', true);
  // Disable display of errors and warnings 
  define('WP_DEBUG_DISPLAY', true);
  @ini_set('display_errors',1);
  // Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
  define('SCRIPT_DEBUG', true);
  // is minified? no
  define('IS_MINIFIED', '');

  /** Define Site & Home url */
  define('WP_HOME','http://ilitazoulay.dev');
  define('WP_SITEURL','http://ilitazoulay.dev');

  /** The name of the database for WordPress */
  define('DB_NAME', 'ilitazoulay');
  /** MySQL database username */
  define('DB_USER', 'root');
  /** MySQL database password */
  define('DB_PASSWORD', 'root');
  /** MySQL hostname */
  define('DB_HOST', 'localhost');
 
}elseif(strstr($_SERVER["HTTP_HOST"], "dev.variable.club") != false){  // Online dev server

    // URLS
  define ('SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/ilitazoulay');
  define ('WP_CONTENT_FOLDERNAME', 'app');
  define ('WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME);
  define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/ilitazoulay/app' );

  /** Define Site & Home url */
  define('WP_HOME','http://dev.variable.club/ilitazoulay');
  define('WP_SITEURL','http://dev.variable.club/ilitazoulay');

  // Enable WP_DEBUG mode
  define('WP_DEBUG', true);
  // Enable Debug logging to the /wp-content/debug.log file
  define('WP_DEBUG_LOG', true);
  // Disable display of errors and warnings 
  define('WP_DEBUG_DISPLAY', true);
  @ini_set('display_errors',1);
  // Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
  define('SCRIPT_DEBUG', true);
  // is minified? no
  define('IS_MINIFIED', '');

  /** The name of the database for WordPress */
  define('DB_NAME', 'dev_ilitazoulay');
  /** MySQL database username */
  define('DB_USER', 'variable');
  /** MySQL database password */
  define('DB_PASSWORD', 'PVQ-bBH-RPL-m3h');
  /** MySQL hostname */
  define('DB_HOST', 'localhost');

  
}else{  // We are in production 

    // URLS
  define ('SITEURL', 'http://' . $_SERVER['HTTP_HOST']);
  define ('WP_CONTENT_FOLDERNAME', 'app');
  define ('WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME);
  define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/app' );
  
  /** Define Site & Home url */
  define('WP_HOME','http://www.ilitazoulay.com');
  define('WP_SITEURL','http://www.ilitazoulay.com');

  // is minified? yes
  define('IS_MINIFIED', '.min');

  /** The name of the database for WordPress */
  define('DB_NAME', 'ilitazoulay');
  /** MySQL database username */
  define('DB_USER', 'variable');
  /** MySQL database password */
  define('DB_PASSWORD', 'PVQ-bBH-RPL-m3h');
  /** MySQL hostname */
  define('DB_HOST', 'localhost');
}


// Enable WP_DEBUG mode
define('WP_DEBUG', false);
// Enable Debug logging to the /wp-content/debug.log file
define('WP_DEBUG_LOG', false);
// Disable display of errors and warnings 
define('WP_DEBUG_DISPLAY', false);
@ini_set('display_errors', 0);
// Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
define('SCRIPT_DEBUG', false);


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z!e+0Uaip>vdPZglOQ 3-5YCv6&~0j!<P:<jc&qNT*<Asrz~~ d.Y)gtODl(Rer1');
define('SECURE_AUTH_KEY',  'lU}pg6*LPSCySz+,8Y-dYEEHrY9|U9el4{N0|,7068D|sb] *yc$XUazUyuSY(%$');
define('LOGGED_IN_KEY',    'M0*I 2At<fl@em`-e>UF`Z39L5YR7by/Xm.dtf+?)|Mu9fcc6h~Kb3|JN60(nfFU');
define('NONCE_KEY',        'G59,=OYv-WWXc$DnP*dFsO4p:()VY{+4nQ+/-DwJc2]{RC8)6|+}a|!d:Ar4H1^-');
define('AUTH_SALT',        '8$Z+M}/Nau5BT^=.XwoAOoo| xv&,_u!wt(2Q=-|W8r.c|lF|xb&9EX~6f-~m8xV');
define('SECURE_AUTH_SALT', 'LMhALr:r*?Y%K)C-5OIw/-p13.e:9-ERTkMv]~NL]my>S.9!XP+|X&U42-ayE$Dz');
define('LOGGED_IN_SALT',   '(-yv~}.:VzXKy&J%7geo8DB|CK/7ET!69LEs<|Q79d.$=hg|9SoSf:A,e_6p]cjb');
define('NONCE_SALT',       '3F;3<2_4KWjbg-Y_(=&+n(EP#?-w`OXl7_4XQ3Te`mo9f*f7lsik&[<MSd^=QxFr');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_ilit_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
