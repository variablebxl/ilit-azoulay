# starter-lab
## starter-Kit + Pattern Lab

Le point de départ des projets sur lesquels nous travaillons chez [Variable](http://www.variable.club).

#### Pour générer le starter-lab
patternlab/core/scripts/generateSite.command

#### Pour compiler le starter-lab
patternlab/core/scripts/startWatcherWithAutoReload.command

## Sources & inspirations
- [Fffunction's Sassaparilla](https://github.com/fffunction/sassaparilla)
- [Twitter Bootstrap](http://getbootstrap.com)
- [Pattern Lab](http://patternlab.io/)